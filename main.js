// 1) Прототипене наслідування в Javascript працює за принципом послідовної передачі 
// властивостей та методів до дочірніх обєктів.

// 2)Похідний конструктор повинен викликати super, щоб виконати 
// його батьківський конструктор, інакше об'єкт для this не буде створено.

class Employee{
constructor(name, age, salary){
    this.name=name;
    this.age=age;
    this.salary=salary;
}

getName(){
    return this.name;
}
setName(){
    this.name;
}

getAge(){
    return this.age;
}
setAge(){
    this.age;
}

getSalary(){
    return this.salary;
}
setSalary(){
    this.salary;
}
}

class Programmer extends Employee{
    constructor(name, age, salary, lang ){
        super(name, age, salary);
        this.lang=lang;
    }
    getSalary(){
        return this.salary*3;
    }
    getLang() {
        return this.lang;
      }
    
}

const programmerOne = new Programmer( "Tom", 27, 2400, "JS, SCSS, HTML");


const programmerTwo = new Programmer( "Miky", 30, 3200, "JS, SCSS, HTML, React, Nodejs");


const programmerThree = new Programmer( "Frenk", 33, 5400, "C++, C#");

console.log(
    programmerOne,
    programmerOne.getSalary(),
    programmerOne.getAge(),
    programmerOne.getName(),
    programmerOne.getLang()
);
console.log(
    programmerTwo,
    programmerTwo.getSalary(),
    programmerTwo.getAge(),
    programmerTwo.getName(),
    programmerTwo.getLang()
);

console.log(
    programmerThree,
    programmerThree.getSalary(),
    programmerThree.getAge(),
    programmerThree.getName(),
    programmerThree.getLang()
);